from typing import Any
import tarfile

DIRTYPE = "dir"
REGTYPE = "file"

TarInfo = tarfile.TarInfo
TarFile = tarfile.TarFile
